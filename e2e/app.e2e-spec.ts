import { ExtractorPanelPage } from './app.po';

describe('extractor-panel App', () => {
  let page: ExtractorPanelPage;

  beforeEach(() => {
    page = new ExtractorPanelPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
