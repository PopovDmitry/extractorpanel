import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router }   from '@angular/router';
import {StreamsService} from '../../services/streams.service';
import {ProjectsService} from '../../services/projects.service';
import 'rxjs/add/operator/switchMap';
import { Location } from '@angular/common';
import {Rules, Selector, Pagination, Stream, Task, Req} from '../../models'
import { PusherService } from '../../services/pusher.service';


@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {

  private data: any = undefined;
  private test: any = {}

  constructor(
    private service: StreamsService,
    private projectService: ProjectsService,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private pusherService: PusherService
  ) { }

  ngOnInit() {

    this.route.params.subscribe((params: Params) =>{
      this.service.allResults(params.stream_id).subscribe( v => {
        this.data = v;
      })  
    })

    
    
  }

}
