import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router }   from '@angular/router';
import {StreamsService} from '../../services/streams.service';
import {ProjectsService} from '../../services/projects.service';
import 'rxjs/add/operator/switchMap';
import { Location } from '@angular/common';
import {Rules, Selector, Pagination, Stream, Task, Req, Merging} from '../../models'
import { PusherService } from '../../services/pusher.service';


@Component({
  selector: 'app-constructor',
  templateUrl: './constructor.component.html',
  styleUrls: ['./constructor.component.scss']
})
export class ConstructorComponent implements OnInit {

  private project_id: number;
  private id: number = 0;
  private tasks: any = [
    {
      key: '',
      value: '',
    }
  ]
  private rules: Rules[] = []
  private options: any = {}
  private project: any = {name: '', id: 0};

  private pagination: Pagination =  new Pagination('', 0, 0);
  private stream: Stream;
  private testID: number = 0;
  private testResult: any = undefined;
  private startSends: number[] = [];
  private startSendsCount: number = 0;
  private readyBar: string = '0%';
  private done: boolean = false;

  constructor(
    private service: StreamsService,
    private projectService: ProjectsService,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private pusherService: PusherService
  ) {

    this.stream = new Stream(null, null, null, null, null, null, null, false);
   }

  ngOnInit() {

    this.pusherService.messagesChannel.bind('App\\Events\\ResultSend', (message) => {
      console.log('msg', message);
      if(message.task_id == this.testID){
        this.testID = 0;
        // получение результатов
        this.service.getResult(message.task_id).subscribe( v => {
          this.testResult = v;
          this.clearTestOnBackEnd(message.stream_id)
        })
      }
    });
    
    this.route.params.subscribe((params: Params) =>{
        this.project_id = params['project_id'];
          // получение проекта
          this.projectService.one(this.project_id).subscribe( project => {
            this.project = project;
          })

          // формирование пустой записи
          this.stream = new Stream(0, '', this.project_id, 0, [], [], this.pagination, false, {});

            var req: Req = new Req('', 'GET', {}, {})
            this.stream.tasks.push(
              new Task(0, 0, this.stream.id, 'htmlParse', 'new', 0, [], req)
            )

          if(params.stream_id == 0){ // новый

          }else{
            this.service.one(params.stream_id).subscribe( (v: any) => {
              
              this.stream = JSON.parse(v.stream);
              // console.log(this.stream);
              this.stream.tasks[0].stream_id = params.stream_id;
              this.stream.id = params.stream_id;
              
            })
          }
    })
  }

  

  // сохранение потока
  save(){
    if(this.stream.id == 0){
      // новый поток 
      this.service.create(this.stream).subscribe( v => {
        this.router.navigate(['/project', this.project_id ,v.id]);
      })
    }else{
      // обновление
      this.service.update(this.stream).subscribe( (v: any) => {
        console.log('update', v);
      })
    }
  }

  // тестирование
  testing(){
    this.testResult = undefined;
    this.service.testing(this.stream).subscribe((v) => {
      console.log('testing', v.id);
      this.testID = v.id;
    })
  }

  clearTestOnBackEnd(stream_id: number):void{
    this.service.clearTest(stream_id).subscribe( r => {
            // console.log('r', r);        
    });
  }

  ngOnDestroy() {
    console.log('destroys');
  }

  // отправка в работу
  start(){
    this.stream.locked = true;
    this.service.sendOnWork(this.stream).subscribe( v => {
      this.startSends = v;
      this.startSendsCount = this.startSends.length;
    })
  }

  // очистка результатов
  clear(){
    this.stream.locked = false;
    this.service.clearResults(this.stream.id).subscribe( v => {})
    this.service.update(this.stream).subscribe( v => {})
  }

  // получение JSON потока
  getJson(stream_id: number){
    this.service.getJson(stream_id).subscribe( v =>{
      console.log(v);
      
    })
  }



}
