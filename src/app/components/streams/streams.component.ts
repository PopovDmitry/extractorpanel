import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import {StreamsService} from '../../services/streams.service';
import {Project, Stream} from '../../models/';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-streams',
  templateUrl: './streams.component.html',
  styleUrls: ['./streams.component.scss']
})
export class StreamsComponent implements OnInit {

  private project_id: number;
  private streams: Stream[];
  private project: any = {name: '', id: 0};

  constructor(
    private service: StreamsService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
  
    this.route.params.subscribe((params: Params) =>{
        this.project_id = params.id;
        this.service.list(params.id).subscribe( (result: any) => {

          this.streams = result.streams.map( (s: any) => {
            var stream:Stream = JSON.parse(s.stream);
            stream.id = s.id
            return stream;
          })

          console.log('streams11', this.streams);
          
          this.project = result.project;
        })
      })
  }

}
