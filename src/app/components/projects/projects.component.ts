import { Component, OnInit } from '@angular/core';
import {ProjectsService} from '../../services/projects.service';
import {StreamsService} from '../../services/streams.service';
import { ActivatedRoute, Params }   from '@angular/router';
import {Project} from '../../models/';
declare var $:any;

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  private projects: Project[];

  private saveData: any = {
    name: ''
  }

  private project_id: number = 0;

  constructor(
    private service: ProjectsService,
    private route: ActivatedRoute
   ) { }

  ngOnInit() {
    this.load();
  }

  // загрузка данных
  load(){
    this.service.list().subscribe((result: Project[]) => {
      this.projects = result;      
    })
  }

  // создание нового проекта
  create(){
    // var pr: Project = new Project(0, this.saveData.name, this.saveData.site, [],[])
    this.service.create(this.saveData).subscribe(result => {
      this.saveData.name = "";
      this.saveData.site = "";
      $('#myModal').modal('hide')
      this.load()
    });
  }

}
