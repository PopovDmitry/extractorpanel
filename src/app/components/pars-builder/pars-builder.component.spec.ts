import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParsBuilderComponent } from './pars-builder.component';

describe('ParsBuilderComponent', () => {
  let component: ParsBuilderComponent;
  let fixture: ComponentFixture<ParsBuilderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParsBuilderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParsBuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
