import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Rules, Selector, Pagination, Stream, Task, Req, Merging} from '../../models'

@Component({
  selector: 'app-pars-builder',
  templateUrl: './pars-builder.component.html',
  styleUrls: ['./pars-builder.component.scss']
})
export class ParsBuilderComponent implements OnInit {

  @Input() stream: Stream;
  @Input() rules: Rules[];

  constructor() { }

  ngOnInit() {
    // setTimeout(()=>{
    //   this.stream.pagination.from = 10;
    // }, 5000)
  }

  // добаление контекста
  addContext(){
    this.stream.tasks[0].rules.push(
      new Rules('', [], '')
    );
  }

  // удаление контекста
  removeContext(rules: Rules[], index){
    if(confirm('Delete Context?')){
      rules.splice(index, 1);
    }
  }

  // доабвление селектора
  addSelector(selector: Selector[]){
    selector.push(
      new Selector('', '')
    )
  }

  // удаление селектора
  removeSelector(selectors: Selector[], index){
    if(confirm('Delete ?')){
      selectors.splice(index, 1)
    }
  }

  // добавление пустого правила
  addEmptyRule(){
    this.rules.push(new Rules('', [], ''));
  }

  // удалить селектор
  remove(s: Selector[] ,index){
    if(confirm()){
      s.splice(index, 1)
    }
  }

  // добавить правило слияния
  addMergingRule(merging: Merging[]){
    merging.push(new Merging('', ''));
  }

  // удалить правило слияния
  removeMergingRule(merging: Merging[], index){
    if(confirm('Delete merging?')){
      merging.splice(index, 1)
    }
  }

}
