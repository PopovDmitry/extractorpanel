import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router }   from '@angular/router';
import {StreamsService} from '../../services/streams.service';
import {ProjectsService} from '../../services/projects.service';
import 'rxjs/add/operator/switchMap';
import { Location } from '@angular/common';
import {Rules, Selector, Pagination, Stream, Task, Req, Merging} from '../../models'
import { PusherService } from '../../services/pusher.service';
import * as _ from 'lodash'


@Component({
  selector: 'app-stream-from-stream',
  templateUrl: './stream-from-stream.component.html',
  styleUrls: ['./stream-from-stream.component.scss']
})


export class StreamFromStreamComponent implements OnInit {

  private project_id: number;
  private stream_parent: Stream;
  private stream: Stream = undefined;
  private project: any = {name: '', id: 0};
  private counts: any;
  private fields: any = [];
  private cntx: any = [];
  private selected: any = {
    cntx: '',
    field: '',
    count: 0
  }
  private demoResults: any = undefined;
  private schema: any = undefined;
  private pagination: Pagination =  new Pagination('', 0, 0);
  private testID: number = 0;
  private testResult: any = undefined;

  constructor(
    private service: StreamsService,
    private projectService: ProjectsService,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private pusherService: PusherService
  ) { 
    // this.stream = new Stream(null, null, null, null, null, null, null, false);
  }

  ngOnInit() {
    this.pusherService.messagesChannel.bind('App\\Events\\ResultSend', (message) => {
      console.log('msg', message);
      if(message.task_id == this.testID){
        this.testID = 0;
        // получение результатов
        this.service.getResult(message.task_id).subscribe( v => {
          this.testResult = v;
          console.log('this.testResult', this.testResult);
          
          this.service.clearTest(message.stream_id).subscribe( r => {
            // console.log('r', r);        
          });
        })
      }
    });

    this.route.params.subscribe((params: Params) =>{
        this.project_id = params.project_id;
        // console.log(params);
        
        this.service.list(this.project_id).subscribe( (result: any) => {
          // console.log('streams', result.project);
          var streams: Stream[] = result.streams.map( (s: any) => {
            // console.log('s', s.stre);
            
            var stream:Stream = JSON.parse(s.stream);
            stream.id = s.id
            return stream;
          })

          this.stream_parent = _.find(streams, (row:Stream) => row.id == params.parent_stream_id)
          


          this.stream = new Stream(0, '', this.project_id, this.stream_parent.id, [], [], this.pagination, false,{});
          var req: Req = new Req('', 'GET', {}, {})
          this.stream.tasks.push(
            new Task(0, 0, this.stream.id, 'htmlParse', 'new', 0, [], req)
          )

          this.project = result.project;

          // получение предварительных данных результатов
          this.service.getSchemaStream(this.stream_parent.id).subscribe( sch => {
            this.schema = sch
            this.cntx = _.keys(this.schema);
            this.stream.selected.cntx = this.cntx[0];
            this.stream.selected.field = this.schema[this.cntx[0]][0];

            this.service.getCountResults(this.stream_parent.id).subscribe( v => {
              this.counts = v;
            });

            if(params.stream_id == 0){ // новый

            }else{
              this.service.one(params.stream_id).subscribe( (v: any) => {
                this.stream = JSON.parse(v.stream);
                this.stream.id = params.stream_id;
                this.stream.tasks[0].stream_id = params.stream_id;
              });
            }
          })
        })
      })
  }

  // тестирование
  testing(){
    this.testResult = undefined;
    // получаем данные о первой строке
    this.service.getTenElements(this.stream_parent.id, this.stream.selected.cntx, this.stream.selected.field).subscribe( tens => {
      this.stream.tasks[0].req.uri = tens[0];
      // console.log(this.stream);
      
      this.service.testing(this.stream).subscribe((v) => {
        console.log('testing', v.id);
        this.testID = v.id;
      })  
    })
    
  }

  // сохранение потока
  save(){
    if(this.stream.id == 0){
      // новый поток 
      this.service.create(this.stream).subscribe( v => {
        this.router.navigate(['/project', this.project_id, 'stream-from-stream', this.stream_parent.id ,v.id]);
      })
    }else{
      // обновление
      this.service.update(this.stream).subscribe( (v: any) => {
        console.log('update', v);
      })
    }
  }

  // получение колличества результатов и его первый элемент для теста
  getDataForStreamForStream(cntx = 'null'){
    this.stream.selected.cntx = cntx;
  }

  onChangeCntx(cntx: string){
    this.stream.selected.cntx = cntx;
    // this.getDataForStreamForStream(cntx);
  }

  onChangeField(field: string){
    this.stream.selected.field = field;
    // console.log('field', this.selected);
    this.service.getCountCNTXElements(this.stream_parent.id, this.stream.selected.cntx, this.stream.selected.field).subscribe(v => {
      this.selected.count = v
    })
  }

  // показать 10 элементов результатат
  showElements(){
    this.service.getTenElements(this.stream_parent.id, this.stream.selected.cntx, this.stream.selected.field).subscribe( v => {
      this.demoResults = v;
    })
  }
  

}
