import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StreamFromStreamComponent } from './stream-from-stream.component';

describe('StreamFromStreamComponent', () => {
  let component: StreamFromStreamComponent;
  let fixture: ComponentFixture<StreamFromStreamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StreamFromStreamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StreamFromStreamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
