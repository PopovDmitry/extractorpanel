// export * from './projects';
// export * from './results';
// export * from './streams';
// модель селекторов
export class Selector{
    constructor(
        public key: string, // ключ в который мы будем сохранять
        public selector: string, // искомые занчения
    ){}
}

// правила слияния
export class Merging{
    constructor(
        public name: string, // название поля куда надо складывать
        public key: string, // по какому ключу надо складывать
    ){}
}

// модель правил для парсинга
export class Rules{
    constructor(
        public context: string, // контекст поиска
        public selectors: Selector[], // селекторы
        public key: string, // ключ в который мы будем сохранять
        public merging: Merging[] = []
    ){}
}

// модель для парсинга правил
export class ParsRule{
    constructor(
        public selector: string,
        public attribute: any,
        public filters: any
    ){}
}

///// модели управления
export class Project{
    constructor(
        public id: number, // индификатор проекта
        public name: string, // название проекта
        public site: string, // адрес сайта
        public streams: Stream[], // потоки
        public results: Result[], // результаты отработки
    ){}
}

export class Stream{
    constructor(
        public id: number, // индификатор потока
        public name: string, // название потока
        public project_id: number, // принадлежность к преокту
        public parentStreamId: number, // вышестоящий поток
        public tasks: Task[], // задачи для обработки
        public fields: any, // поля для доплнения
        public pagination: Pagination, // настройки пагинации
        public locked: boolean = false, // заперта ли очередь
        public selected?: any, // если есть выбранные поля
    ){}
}

export class Pagination{
    constructor(
        public url: string, // урл
        public from: number = 0, // откуда начинать
        public to: number = 0, // докуда
    ){}
}


// запрос
export class Req{
    constructor(
        public uri: string, // урл для получения данных
        public method: string = 'GET', // тип получаемых данных
        public params: any = {}, // ключи и значения для отправки данных
        public headers: any = {}, // значения заголовков
    ){}
}

// задачи
export class Task{
    constructor(
        public id: number, // уникальный индификатор задачи
        public parentTaskId: number = 0, // над стоящая задача
        public stream_id: number, // индификатор к какому потоку принедлежит задача
        public type: string = "htmlParse", // тип задачи, может принимать знаечение, download или htmlParse
        public status: string = "new", // статус задачи, может принимать знаечение, new, process, done
        public delay: number = 0, // завесание перед выполнением в милисекундах
        public rules: Rules[], // правила для выполнения
        public req: Req, // запрос
    ){}
}

// результат
export class Result{
    constructor(
        public id: number, // индификатор результата
        public task_id: number = 0, // владелец результата
        public result: any, // результат
        public timing: number, // время выполнения в милисекундах
        public worker: string, // индификатор выполняющего
        public error: any, 
    ){}
}