import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { StreamsComponent } from './components/streams/streams.component';
import { ResultsComponent } from './components/results/results.component';
import { ConstructorComponent } from './components/constructor/constructor.component';
// services

import { ApiService } from './services/api.service';
import { ProjectsService } from './services/projects.service';
import { StreamsService } from './services/streams.service';
import { ResultsService } from './services/results.service';
import { PusherService } from './services/pusher.service';
import { ParsersComponent } from './components/parsers/parsers.component';
import { StreamFromStreamComponent } from './components/stream-from-stream/stream-from-stream.component';
import { KeysPipe } from './keys.pipe';
import { ParsBuilderComponent } from './components/pars-builder/pars-builder.component';
// import {TJsonViewerModule} from 't-json-viewer';



@NgModule({
  declarations: [
    AppComponent,
    ProjectsComponent,
    StreamsComponent,
    ResultsComponent,
    ConstructorComponent,
    ParsersComponent,
    StreamFromStreamComponent,
    KeysPipe,
    ParsBuilderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    // TJsonViewerModule
  ],
  providers: [ApiService, ProjectsService, StreamsService, ResultsService, PusherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
