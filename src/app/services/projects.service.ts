import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ApiService } from './api.service';
import { Observable } from "rxjs/Rx";
import {Project} from '../models/';

@Injectable()
export class ProjectsService extends ApiService {
  constructor(private http2: Http) { 
    super(http2);
  }

  // получение проектов
  list(): Observable<Project[]>{
      return this.get('projects')
  }

  // создание проекта
  create(data: Project){
      return this.post('projects', data);
  }

  // получение одного проекта
  one(project_id: number): Observable<Project>{
      return this.get('projects/' + project_id);
  }
  

}
