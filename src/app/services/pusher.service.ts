import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
declare const Pusher: any;

@Injectable()
export class PusherService {
  pusher: any;
  messagesChannel: any;

  constructor() { 
    this.pusher = new Pusher(environment.pusher.key, {
      cluster: 'eu',
      encrypted: true,
      authEndpoint: 'http://extractor.pushtools.ru/api/auth',
      // authEndpoint: 'http://extractor.pushtools.ru/broadcasting/auth',
      auth: {
        headers: {
          'AUTH-key': "3104250"
        }
      }

    });

    this.messagesChannel = this.pusher.subscribe('presence-result');
  }

}
