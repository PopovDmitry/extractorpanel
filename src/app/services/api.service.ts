import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable, Subject } from "rxjs/Rx";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class ApiService {

  private serverUrl = 'http://extractor.pushtools.ru/api/';

  constructor(private http: Http) { }

  // получение методом GET
  get(method: string): Observable<any>{
        let url: string = this.serverUrl + method;
        return this.http
                .get(url, this.createRequestOptions())
                .map((res: Response) => {
                    var r = res.json();
                    if(r.data != undefined){
                        return r.data;
                    }
                    return r;
                });
  }

  // отправка методом POST
  post(method: string, data: any): Observable<any>{
    let url: string = this.serverUrl + method;
    return this.http
                .post(url, JSON.stringify(data), this.createRequestOptions())
                .map((res: Response) => {
                    var r = res.json();
                    if(r.data != undefined){
                        return r.data;
                    }
                    return r;
                });
  }

  // отправка методом PUT
  put(method: string, data: any): Observable<any>{
    let url: string = this.serverUrl + method;
    return this.http
                .put(url, JSON.stringify(data), this.createRequestOptions())
                .map((res: Response) => {
                    var r = res.json();
                    if(r.data != undefined){
                        return r.data;
                    }
                    return r;
                });
  }



    private createRequestOptions() {
        let headers = new Headers();
        headers.append("AUTH-key", '3104250');
        headers.append("Content-Type", "application/json");
        let options = new RequestOptions({ headers: headers });
        return options;
    }

}
