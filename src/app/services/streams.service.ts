import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ApiService } from './api.service';
import { Observable } from "rxjs/Rx";
import {Stream} from '../models/';

@Injectable()
export class StreamsService extends ApiService {

  constructor(private http2: Http) { 
    super(http2);
  }

  // получение списка потоков
  list(project_id: number):Observable<Stream[]>{
    return this.get('streams/project/' + project_id);
  }

  // получение данных конструктора
  constr(project_id: number, id: number):Observable<Stream[]>{
    return this.get('streams/constructor/' + id)
  }

  // создание потока
  create(stream: Stream):Observable<any>{
    return this.post('streams', stream);
  }

  one(id: number):Observable<Stream>{
    return this.get('streams/constructor/' + id);
  }

  // обновление потока
  update(stream: Stream):Observable<any>{
    return this.put('streams', stream);
  }

  // тестирование потока
  testing(stream: Stream):Observable<any>{
    this.update(stream).subscribe( v => {});
    return this.get('streams/testing/' + stream.id)
  }

  // получение конкретного результата
  getResult(id: number):Observable<any>{
    return this.get('result/' + id)
  }

  // очистка теста
  clearTest(stream_id: number):Observable<any>{
    return this.get('clear/streams/results/' + stream_id);
  }

  // отправка результатов в работу
  sendOnWork(stream: Stream):Observable<any>{
    this.update(stream).subscribe( v => {});
    return this.get('streams/work/' + stream.id);
  }

  allResults(stream_id: number):Observable<any>{
    return this.get('streams/results/' + stream_id);
  }

  // очистка результатов
  clearResults(stream_id: number):Observable<any>{
    return this.get('clear/streams/results/' + stream_id);
  }

  // получение json
  getJson(stream_id: number):Observable<any>{
    return this.get('streams/json/' + stream_id);
  }

  // получение кооличества результатов в потоке
  getCountResults(stream_id: number):Observable<any>{
    return this.get('streams/results/count/' + stream_id);
  }

  // получение одного результата конкретного потока
  getResultsFromStream(stream_id: number, cntx:string = "null", limit:number = 1, offset: number = 0):Observable<any>{
    return this.get('streams/results/limited/' + stream_id + '/'+ cntx + '/' + limit + '/' + offset);
  }

  // получение списка контекстов в результатах
  getContectFromResults(stream_id: number):Observable<any>{
    return this.get('streams/results/contexts/' + stream_id);
  }

  // получаем количество элементов по контексту и полю
  getCountCNTXElements(stream_id: number, cntx: string = "null", field:string = ""):Observable<any>{
    return this.get('streams/results/contexts/count/' + stream_id + '/' +cntx + '/' + field );
  }

  // показать 10 эелементов результата
  getTenElements(stream_id: number, cntx: string = "null", field:string = ""):Observable<any>{
    return this.get('streams/results/contexts/demo/' + stream_id + '/' +cntx + '/' + field );
  }

  // получение структуры внутрених полей
  getSchemaStream(stream_id: number):Observable<any>{
    return this.get('streams/results/schema/' + stream_id );
  }

}
