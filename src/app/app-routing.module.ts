import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectsComponent } from './components/projects/projects.component';
import { StreamsComponent } from './components/streams/streams.component';
import { ConstructorComponent } from './components/constructor/constructor.component';
import { ResultsComponent } from './components/results/results.component';
import { StreamFromStreamComponent } from './components/stream-from-stream/stream-from-stream.component';


const routes: Routes = [
  {
    path: '',
    component: ProjectsComponent
  },
  {
    path: 'project/:id',
    component: StreamsComponent
  },
  {
    path: 'streams/:id',
    component: StreamsComponent
  },
  {
    path: 'project/:project_id/:stream_id',
    component: ConstructorComponent
  },
  {
    path: 'project/:project_id/:stream_id/result',
    component: ResultsComponent
  },
  {
    path: 'project/:project_id/stream-from-stream/:parent_stream_id/:stream_id',
    component: StreamFromStreamComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
